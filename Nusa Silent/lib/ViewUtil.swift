//
//  ViewUtil.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 22/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation
import UIKit
import ACProgressHUD_Swift

class ViewUtil {
    
    static func customAlert(message: String, controller: UIViewController, alertAction: UIAlertAction){
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let alertCancel = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alert.addAction(alertAction)
        alert.addAction(alertCancel)
        controller.present(alert, animated: true, completion: nil)
    }
    
    static func showDefaultProgressHud() -> ACProgressHUD {
        let progress = ACProgressHUD.shared
        progress.progressText = ""
        progress.showHudAnimation = .growIn
        progress.dismissHudAnimation = .growOut
        progress.enableBlurBackground = false
        progress.enableBackground = true
        progress.hudBackgroundColor = .white
        progress.indicatorColor = #colorLiteral(red: 0.2078431373, green: 0.5647058824, blue: 0.9176470588, alpha: 1)
        progress.showHUD()
        return progress
    }
}
