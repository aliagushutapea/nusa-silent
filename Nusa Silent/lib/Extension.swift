//
//  Extension.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation
import GoogleMaps
import MediaPlayer

var previousLocation: CLLocation?
extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        guard status == .authorizedWhenInUse || status == .authorizedAlways else {
            return
        }
        
        self.locationManager.startUpdatingLocation()
        self.mapView?.isMyLocationEnabled = true
        self.mapView?.settings.myLocationButton = true
        self.mapView?.settings.compassButton = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if self.timer == nil {
            guard let location = locations.first else {
                return
            }
            self.beginNewBackgroundTask()
            self.mapView?.camera = GMSCameraPosition(target: location.coordinate, zoom: 17.0, bearing: 0, viewingAngle: 0)
            DatabaseManager.setMyLastLocation(location: location)
            requestNearbyPlace(radius: 20)
            self.locationManager.stopUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.beginNewBackgroundTask()
        self.locationManager.stopUpdatingLocation()
    }
    
    
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        reverseGeocodeCoordinate(position.target)
    }
}

let cacheImage = NSCache<AnyObject, AnyObject>()
extension UIImageView {
    func loadImageUsingCacheUrlString(urlString: String) {
        self.image = nil
        
        if let imageCache = cacheImage.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageCache
            return
        }
        let url = URLRequest(url: URL(string: (urlString))!)
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil{
                print("loadImage error")
                return
            }
            
            DispatchQueue.main.async {
                if let downloadImage = UIImage(data: data!){
                    cacheImage.setObject(downloadImage, forKey: urlString as AnyObject)
                    self.image = downloadImage
                }
            }
            }.resume()
    }
}

extension MPVolumeView {
    static func setVolume(_ volume: Float) {
        let volumeView = MPVolumeView()
        let slider = volumeView.subviews.first(where: { $0 is UISlider }) as? UISlider
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.01) {
            slider?.value = volume
        }
    }
}
