//
//  NetworkManager.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 21/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import GoogleMaps

class NetworkManager {
    
    static let TAG = "NetworkManager"
    
    class  func requestNearbyPlace(radius: Int, closure: @escaping(_ placeGoogle: PlaceGoogle?) -> ()) {
        if GeneralLib.isConnectedToNetwork() {
            DatabaseManager.fetchMyLastLocation(){ myLocation in
                let url = BankString.urlNearbyPlace + String(format: "%.7f", myLocation?.coordinate.latitude ?? 0.0) + "," + String(format: "%.7f", myLocation?.coordinate.longitude ?? 0.0) + BankString.parameterRadius + String(radius) + BankString.parameterKey
                
                AF.request(url)
                    .validate()
                    .responseJSON{ response in
                        switch response.result {
                        case .success(let value):
                            let placeGoogle = PlaceGoogle()
                            let json = JSON(value)
                            placeGoogle.nextPageToken = json["next_page_token"].stringValue
                            placeGoogle.status = json["status"].stringValue
                            
                            var listPlace = [Place]()
                            let placeArray = json["results"].array
                            
                            placeArray?.forEach({ (placeJson) in
                                let place = Place()
                                place.id = placeJson["id"].stringValue
                                place.placeId = placeJson["place_id"].stringValue
                                place.name = placeJson["name"].stringValue
                                place.icon = placeJson["icon"].stringValue
                                place.reference = placeJson["reference"].stringValue
                                place.rating = placeJson["rating"].doubleValue
                                place.userRatingsTotal = placeJson["user_ratings_total"].intValue
                                place.vicinity = placeJson["vicinity"].stringValue
                                place.scope = placeJson["scope"].stringValue
                                
                                let geometryJson = placeJson["geometry"]
                                let locationJson = geometryJson["location"]
                                let location = Location()
                                location.lat = locationJson["lat"].doubleValue
                                location.lng = locationJson["lng"].doubleValue
                                
                                let viewPortJson = geometryJson["viewport"]
                                
                                let southWestJson = viewPortJson["southwest"]
                                let southWest = SouthWest()
                                southWest.lat = southWestJson["lat"].doubleValue
                                southWest.lng = southWestJson["lng"].doubleValue
                                
                                let northEastJson = viewPortJson["northeast"]
                                let northEast = NorthEast()
                                northEast.lat = northEastJson["lat"].doubleValue
                                northEast.lng = northEastJson["lng"].doubleValue
                                
                                let viewPort = ViewPort()
                                viewPort.northEast = northEast
                                viewPort.southWest = southWest
                                
                                let geometry = Geometry()
                                geometry.location = location
                                geometry.viewPort = viewPort
                                
                                place.geometry = geometry
                                
                                let plusCodeJson = placeJson["plus_code"]
                                let plusCode = PlusCode()
                                plusCode.compoundCode = plusCodeJson["compound_code"].stringValue
                                plusCode.globalCode = plusCodeJson["global_code"].stringValue
                                
                                place.plusCode = plusCode
                                
                                let photos = placeJson["photos"].array
                                var listPhotos = [Photo]()
                                photos?.forEach({ (photoJson) in
                                    let photo = Photo()
                                    photo.width = photoJson["width"].intValue
                                    photo.height = photoJson["height"].intValue
                                    photo.photoReference = photoJson["photo_reference"].stringValue
                                    listPhotos.append(photo)
                                })
                                
                                place.photos = listPhotos
                                
                                let types = placeJson["types"].arrayObject
                                var listTypes = [String]()
                                types?.forEach({ (type) in
                                    listTypes.append(type as! String)
                                })
                                
                                place.types = listTypes
                                
                                listPlace.append(place)
                            })
                            placeGoogle.result = listPlace
                            closure(placeGoogle)
                            break
                        case .failure(_):
                            closure(nil)
                            break
                        }
                }
            }
        } else {
            if radius == 20 {
                DatabaseManager.fetchAllFavouritePlaces { (listPlace) in
                    DatabaseManager.fetchMyLastLocation(closure: { (myLocation) in
                        var listFavouritePlace = [Place]()
                        listPlace?.forEach({ (place) in
                            let locationFavourite = CLLocation(latitude: place.geometry?.location?.lat ?? 0.0, longitude: place.geometry?.location?.lng ?? 0.0)
                            print(myLocation!.distance(from: locationFavourite))
                            if Int(myLocation!.distance(from: locationFavourite)) <= radius {
                                listFavouritePlace.append(place)
                            }
                        })
                        let placeGoogle = PlaceGoogle()
                        placeGoogle.result = listFavouritePlace
                        closure(placeGoogle)
                    })
                }
            }
        }
    }
    
    class func requestDistance(myLocation: CLLocation, location: Location, closure: @escaping(_ destination: Destination?)-> ()) {
        checkConnection()
        let url = BankString.urlDistance + String(format: "%.7f", myLocation.coordinate.latitude) + "," + String(format: "%.7f", myLocation.coordinate.longitude) + BankString.parameterUrlDistanceDestination + String(format: "%.7f", location.lat) + "," + String(format: "%.7f", location.lng) + BankString.parameterUrlDistanceKey
        
        AF.request(url)
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success(let value):
                    let json = JSON(value)
                    let destination = Destination()
                    destination.address = json["destination_addresses"].stringValue
                    
                    let rowJsonArray = json["rows"].array
                    let rowJson = rowJsonArray![0]
                    let elementJsonArray = rowJson["elements"].array
                    let elementJson = elementJsonArray![0]
                    let distanceJson = elementJson["distance"]
                    destination.distanceText = distanceJson["text"].stringValue
                    destination.distanceValue = distanceJson["value"].intValue
                    
                    let durationJson = elementJson["duration"]
                    destination.durationText = durationJson["text"].stringValue
                    destination.durationValue = durationJson["value"].intValue
                    
                    closure(destination)
                    break
                case .failure(_):
                    closure(nil)
                    break
                }
        }
    }
    
    static func checkConnection(){
        guard GeneralLib.isConnectedToNetwork() else {
            return
        }
    }
}
