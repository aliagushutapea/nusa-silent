//
//  BankString.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation

class BankString {
    
    // url
    static let key = "AIzaSyABuuIwWUqVuD7IpEg7RyhqM4stcqt235E"
    static let baseUrl = "https://maps.googleapis.com"
    static let urlNearbyPlace = baseUrl + "/maps/api/place/nearbysearch/json?location="
    static let parameterRadius = "&radius="
    static let parameterKey = "&sensor=true&types=mosque&key=" + key
    static let urlPhotoReference = baseUrl + "/maps/api/place/photo?maxwidth=768&photoreference="
    static let parameterUrlPhotoReference = "&key=" + key
    static let urlDistance = baseUrl +  "/maps/api/distancematrix/json?units=metric&origins=" // + coordinate mylocation
    static let parameterUrlDistanceDestination = "&destinations=" // + coordinate tempat favourite
    static let parameterUrlDistanceKey = "&key=" + key

    static let map = "MAP"
    static let nearby = "NearBy Places"
    static let favourite = "Favourite"
    static let name = "NAME"
    static let rating = "RATING"
    static let confirmToDelete = "Are you sure to remove this favourite place?"
    
}
