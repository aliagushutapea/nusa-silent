//
//  FavouritePlace+CoreDataProperties.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//
//

import Foundation
import CoreData


extension FavouritePlace {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<FavouritePlace> {
        return NSFetchRequest<FavouritePlace>(entityName: "FavouritePlace")
    }

    @NSManaged public var id: String?
    @NSManaged public var id_place: String?
    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double
    @NSManaged public var name: String?
    @NSManaged public var reference: String?
    @NSManaged public var type: String?
    @NSManaged public var photo_reference: String?
    @NSManaged public var rating: Double
    @NSManaged public var user_rating_total: Int64
    @NSManaged public var address: String?
    @NSManaged public var distance_text: String?
    @NSManaged public var distance_value: Int
    @NSManaged public var duration_text: String?
    @NSManaged public var duration_value: Int

}
