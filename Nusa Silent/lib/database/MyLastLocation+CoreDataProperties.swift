//
//  MyLastLocation+CoreDataProperties.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//
//

import Foundation
import CoreData


extension MyLastLocation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<MyLastLocation> {
        return NSFetchRequest<MyLastLocation>(entityName: "MyLastLocation")
    }

    @NSManaged public var latitude: Double
    @NSManaged public var longitude: Double

}
