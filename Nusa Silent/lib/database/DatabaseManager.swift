//
//  DatabaseManager.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation
import CoreData
import SwiftyJSON
import GoogleMaps

class DatabaseManager {
    
    static let TAG = "DatabaseManger"
    
    static let myLastLocationRequest: NSFetchRequest<MyLastLocation> = MyLastLocation.fetchRequest()
    static let favouritePlaceReqeust: NSFetchRequest<FavouritePlace> = FavouritePlace.fetchRequest()
    
    static func setMyLastLocation(location: CLLocation){
        do {
            var myLastLocation: MyLastLocation?
            let myLastLocations = try PersistenceService.context.fetch(myLastLocationRequest)
            if myLastLocations.count > 0 {
                myLastLocation = myLastLocations[0] as MyLastLocation
            } else {
                myLastLocation = MyLastLocation(context: PersistenceService.context)
            }
            myLastLocation?.latitude = location.coordinate.latitude
            myLastLocation?.longitude = location.coordinate.longitude
            PersistenceService.saveContext()
        } catch _{
            print("error when save my last location")
        }
    }
    
    static func fetchMyLastLocation(closure: @escaping(_ location: CLLocation?) -> ()) {
        do {
            let myLastLocations = try PersistenceService.context.fetch(myLastLocationRequest)
            if myLastLocations.count > 0 {
                let myLastLocation = myLastLocations[0] as MyLastLocation
                closure(CLLocation(latitude: myLastLocation.latitude, longitude: myLastLocation.longitude))
            } else {
                closure(CLLocation(latitude: 0.0, longitude: 0.0))
            }
        } catch _{
            print("error when fetch my last location")
            closure(CLLocation(latitude: 0.0, longitude: 0.0))
        }
    }
    
    static func saveFavouritePlace(place: Place, completion: (() -> Void)){
        do {
            var favouritePlace: FavouritePlace?
            let location = place.geometry?.location
            let favouritePlaceReqeust: NSFetchRequest<FavouritePlace> = FavouritePlace.fetchRequest()
            favouritePlaceReqeust.predicate = NSPredicate(format: "id = %@", place.id!)
            let favouritePlaces = try PersistenceService.context.fetch(favouritePlaceReqeust)
            if favouritePlaces.count > 0 {
                favouritePlace = favouritePlaces[0] as FavouritePlace
            } else {
                favouritePlace = FavouritePlace(context: PersistenceService.context)
            }
            favouritePlace?.id = place.id
            favouritePlace?.id_place = place.placeId
            favouritePlace?.latitude = location?.lat ?? 0.0
            favouritePlace?.longitude = location?.lng ?? 0.0
            favouritePlace?.name = place.name
            favouritePlace?.reference = place.reference
            if place.photos.count > 0 {
                favouritePlace?.photo_reference = place.photos[0].photoReference
            }
            favouritePlace?.rating = place.rating
            favouritePlace?.user_rating_total = Int64(place.userRatingsTotal)
            favouritePlace?.address = place.address
            favouritePlace?.distance_text = place.distanceText
            favouritePlace?.distance_value = place.distanceValue
            favouritePlace?.duration_text = place.durationText
            favouritePlace?.duration_value = place.durationValue
            PersistenceService.saveContext()
            completion()
        } catch _{
            print("error when save favourite place")
        }
    }
    
    static func removeFavouritePlace(place: Place, completion: (() -> Void)) {
        do {
            
            let favouritePlaces = try PersistenceService.context.fetch(favouritePlaceReqeust)
            if favouritePlaces.count > 0 {
                favouritePlaces.forEach { (favouritePlace) in
                    if favouritePlace.id == place.id {
                        PersistenceService.context.delete(favouritePlace)
                    }
                }
                PersistenceService.saveContext()
            }
//            let favouritePlaceReqeust: NSFetchRequest<FavouritePlace> = FavouritePlace.fetchRequest()
//            favouritePlaceReqeust.predicate = NSPredicate(format: "id = %@", place.id!)
//            let favouritePlaces = try PersistenceService.context.fetch(favouritePlaceReqeust)
//            if favouritePlaces.count > 0 {
//                favouritePlaces.forEach { (favouritePlace) in
//                    PersistenceService.context.delete(favouritePlace)
//                }
//            }
            completion()
        } catch _{
            print("error when remove favourite place")
        }
    }
    
    static func fetchAllIdFavouritePlaces(closure: @escaping(_ listIdPlace: [String]?) -> ()) {
        do {
            var listIdPlace = [String]()
            let favouritePlaces = try PersistenceService.context.fetch(favouritePlaceReqeust)
            if favouritePlaces.count > 0 {
                favouritePlaces.forEach { (favouritePlace) in
                    listIdPlace.append(favouritePlace.id ?? "")
                }
                print(listIdPlace)
                closure(listIdPlace)
            } else {
                closure(nil)
            }
        } catch _{
            print("error when fetch my last location")
        }
    }
    
    static func fetchAllFavouritePlaces(closure: @escaping(_ listPlace: [Place]?) -> ()) {
        do {
            var listPlace = [Place]()
            let favouritePlaces = try PersistenceService.context.fetch(favouritePlaceReqeust)
            if favouritePlaces.count > 0 {
                favouritePlaces.forEach { (favouritePlace) in
                    let place = Place()
                    place.id = favouritePlace.id
                    place.placeId = favouritePlace.id_place
                    
                    let location = Location()
                    location.lat = favouritePlace.latitude
                    location.lng = favouritePlace.longitude
                    
                    let geometry = Geometry()
                    geometry.location = location
                    
                    place.geometry = geometry
                    place.name = favouritePlace.name
                    place.reference = favouritePlace.reference
                    
                    if let photoReference = favouritePlace.photo_reference {
                        let photo = Photo()
                        photo.photoReference = photoReference
                        place.photos = [photo]
                    }
                    place.rating = favouritePlace.rating
                    place.userRatingsTotal = Int(favouritePlace.user_rating_total)
                    place.address = favouritePlace.address
                    place.distanceText = favouritePlace.distance_text
                    place.distanceValue = favouritePlace.distance_value
                    place.durationValue = favouritePlace.duration_value
                    place.durationText = favouritePlace.duration_text
                    
                    listPlace.append(place)
                }
            }
            closure(listPlace)
        } catch _{
            print("error when fetch my last location")
            closure(nil)
        }
    }
}
