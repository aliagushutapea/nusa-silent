//
//  PlaceMarker.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 21/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit
import GoogleMaps

class PlaceMarker: GMSMarker {
    
    let place: Place
    
    init(place: Place){
        self.place = place
        super.init()
        let location = CLLocation(latitude: (place.geometry?.location!.lat)!, longitude: (place.geometry?.location!.lng)!)
        position = location.coordinate
        icon = GMSMarker.markerImage(with: #colorLiteral(red: 0.2078431373, green: 0.5647058824, blue: 0.9176470588, alpha: 1))
        groundAnchor = CGPoint(x: 0.5, y: 1)
        appearAnimation = .pop
    }

}
