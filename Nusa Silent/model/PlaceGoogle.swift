//
//  Place.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 18/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation
import SwiftyJSON

class PlaceGoogle {
    var nextPageToken: String?
    var result: [Place] = []
    var status: String?
}

class Place {
    var geometry: Geometry?
    var icon: String?
    var id: String?
    var name: String?
    var photos: [Photo] = []
    var placeId: String?
    var plusCode: PlusCode?
    var rating: Double = 0.0
    var reference: String?
    var scope: String?
    var types: [String] = []
    var userRatingsTotal: Int = 0
    var vicinity: String?
    var address: String?
    var distanceText: String?
    var distanceValue: Int = 0
    var durationText: String?
    var durationValue: Int = 0
}

class Geometry {
    var location: Location?
    var viewPort: ViewPort?
}

class Location {
    var lat: Double = 0.0
    var lng: Double = 0.0
}

class ViewPort {
    var southWest: SouthWest?
    var northEast: NorthEast?
}

class SouthWest {
    var lat: Double = 0.0
    var lng: Double = 0.0
}

class NorthEast {
    var lat: Double = 0.0
    var lng: Double = 0.0
}

class Photo {
    var height: Int = 0
    var width: Int = 0
    var photoReference: String?
    var htmlAttributes: [String] = []
}

class PlusCode {
    var compoundCode: String?
    var globalCode: String?
}
