//
//  Destination.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 21/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import Foundation

class Destination {
    var address: String?
    var distanceText: String?
    var distanceValue: Int = 0
    var durationText: String?
    var durationValue: Int = 0
}
