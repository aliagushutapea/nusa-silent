//
//  FavouriteTableViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit



class FavouriteCollectionViewController: BasePlaceCollectionViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.fetchPlace1()
        }
    }
    
    func fetchPlace1() {
        //super.fetchPlace()
        let progress = ViewUtil.showDefaultProgressHud()
        DatabaseManager.fetchAllFavouritePlaces { (listPlace) in
            if listPlace != nil{
                self.viewNothing.isHidden = true
                self.listPlace = listPlace!
                DatabaseManager.fetchAllIdFavouritePlaces(closure: { (listIdPlace) in
                    self.listIdFavourite = listIdPlace ?? self.listIdFavourite
                    self.listPlace = self.listPlace.sorted(by: { (place0, place1) -> Bool in
                        return place0.distanceValue < place1.distanceValue
                    })
                    self.collectionView.reloadData()
                    progress.hideHUD()
                })
            }
        }
    }
    
    override func saveFavourite(view: UIView) {
        let index = view.tag
        let place = listPlace[index]
        let cell = collectionView.cellForItem(at: IndexPath(row: index, section: 0)) as! PlaceCollectionViewCell
        DispatchQueue.main.async {
            if self.listIdFavourite.contains(place.id!) {
                let alertAction = UIAlertAction(title: "Yes", style: .default, handler: { (alertAction) in
                    UIView.transition(with: cell.imageFavourite, duration: 0.4, options: .transitionFlipFromLeft, animations: {
                        cell.imageFavourite.image = UIImage(named: "border_favourite")
                    }, completion: { (isComplete) in
                        DatabaseManager.removeFavouritePlace(place: place, completion: {
                            self.collectionView.performBatchUpdates({
                                self.listIdFavourite = self.listIdFavourite.filter { $0 != place.id}
                                self.listPlace = self.listPlace.filter({ (place1) -> Bool in
                                    return place1.id != place.id
                                })
                                self.collectionView.deleteItems(at: [IndexPath(item: index, section: 0)])
                                //self.collectionView.reloadData()
                            }, completion: { _ in
                                self.collectionView.reloadItems(at: self.collectionView.indexPathsForVisibleItems)
                            })
                        })
                    })
                })
                ViewUtil.customAlert(message: BankString.confirmToDelete, controller: self, alertAction: alertAction)
            }
        }
    }

}
