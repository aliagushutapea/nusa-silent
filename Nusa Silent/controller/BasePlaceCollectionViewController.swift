//
//  NearByTableViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class BasePlaceCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {

    var listPlace: [Place] = []
    var listIdFavourite: [String] = []
    let cellId = "cellId"
    var viewNothing: UIView = {
        let view = UIView()
        view.isHidden = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageView: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "empty_box_open"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(viewNothing)
        viewNothing.addSubview(imageView)
        
        viewNothing.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        viewNothing.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        viewNothing.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        viewNothing.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        
        imageView.centerXAnchor.constraint(equalTo: viewNothing.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: viewNothing.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 100).isActive = true

        setupCollectionView()
        fetchPlace()
    }
    
    func setupCollectionView(){
        collectionView.backgroundColor = UIColor(white: 0.85, alpha: 1)
        collectionView.register(PlaceCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
    }
    
    func fetchPlace(){
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        UIView.transition(with: self.viewNothing, duration: 0.4, options: UIView.AnimationOptions.transitionFlipFromTop, animations: {
            self.viewNothing.isHidden = self.listPlace.count > 0 ? true : false
        }, completion: nil)
        return listPlace.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! PlaceCollectionViewCell
        cell.nearByPlaceCollection = self
        let place = listPlace[indexPath.row]
        cell.viewFavourite.tag = indexPath.item
        cell.viewRoute.tag = indexPath.row
        if place.photos.count > 0 {
            let photo = place.photos[0]
            let url = BankString.urlPhotoReference + (photo.photoReference ?? " ") + BankString.parameterUrlPhotoReference
            cell.imageReference.loadImageUsingCacheUrlString(urlString: url)
        } else {
            cell.imageReference.image = UIImage(named: "image_not_found")
        }
        cell.valueName.text = place.name
        if place.rating > 0.0 {
            cell.valueRating.text = "\(place.rating) OF " + "\(place.userRatingsTotal) USERS"
        } else {
            cell.labelRating.isHidden = true
            cell.valueRating.isHidden = true
        }
        
        if listIdFavourite.contains(place.id!) {
            cell.imageFavourite.image = UIImage(named: "favourite")
        } else {
            cell.imageFavourite.image = UIImage(named: "border_favourite")
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 400)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func saveFavourite(view: UIView){
    }
    
    func navigateToRoute(view: UIView){
        let place = listPlace[view.tag]
        let destination = place.geometry?.location
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if let urlDestination = URL.init(string: "comgooglemaps://?saddr=&daddr=" + String(format: "%.7f", destination!.lat) + "," + String(format: "%.7f", destination!.lng) + "&directionsmode=driving") {
                    UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
                }
            }
            else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(destination: destination!)
            }
        }
        else
        {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser(destination: destination!)
        }
    }
    
    func openTrackerInBrowser(destination: Location){
        if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=&daddr=\(destination.lat),\(destination.lng)&directionsmode=driving") {
            UIApplication.shared.open(urlDestination, options: [:], completionHandler: nil)
        }
    }
    
}
