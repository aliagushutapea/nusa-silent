//
//  NearByTableViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class NearByPlaceCollectionViewController: UICollectionViewController {

    var listPlace: [Place] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        requestNearbyPlace()
    }
    
    func requestNearbyPlace(){
        NetworkManager.requestNearbyPlace { placeGoogle in
            
        }
    }
    
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return listPlace.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as 
    }
    
}
