//
//  MapViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import AVFoundation
import MediaPlayer

class MapViewController: UIViewController {

    lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.requestWhenInUseAuthorization()
        manager.requestAlwaysAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.allowsBackgroundLocationUpdates = true
        return manager
    }()
    var currentLocation: CLLocation?
    var cameraPosition: GMSCameraPosition?
    var mapView: GMSMapView?
    var listPlace = [Place]()
    let backgroundTimer = 30.0 // restart location manager every 150 seconds
    let UPDATE_SERVER_INTERVAL = 60 * 60
    var timer:Timer?
    var currentBgTaskId : UIBackgroundTaskIdentifier?
    var lastLocationDate : NSDate = NSDate()
    var previousVolume: Float = 0.0
    
    let viewTop: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 10
        view.layer.masksToBounds = true
        view.backgroundColor = UIColor(white: 0.95, alpha: 0.7)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    let labelAddress: UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 0
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 12)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let labelRinger: UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 1
        lb.text = "Ringger Mode"
        lb.textAlignment = .center
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let labelModeRinger: UILabel = {
        let lb = UILabel()
        lb.numberOfLines = 1
        lb.text = "Normal"
        lb.textColor = #colorLiteral(red: 0.2078431373, green: 0.5647058824, blue: 0.9176470588, alpha: 1)
        lb.textAlignment = .center
        lb.font = UIFont.boldSystemFont(ofSize: 15)
        lb.translatesAutoresizingMaskIntoConstraints = false
        return lb
    }()
    
    let divider: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let viewWidht = self.view.frame.width
        setupMap()
        view.addSubview(viewTop)
        viewTop.addSubview(labelAddress)
        viewTop.addSubview(labelRinger)
        viewTop.addSubview(labelModeRinger)
        viewTop.addSubview(divider)
        
        viewTop.topAnchor.constraint(equalTo: view.topAnchor, constant: 5).isActive = true
        viewTop.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 10).isActive = true
        viewTop.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -10).isActive = true
        viewTop.heightAnchor.constraint(greaterThanOrEqualToConstant: 90).isActive = true
        
        labelAddress.topAnchor.constraint(equalTo: viewTop.topAnchor).isActive = true
        labelAddress.leftAnchor.constraint(equalTo: viewTop.leftAnchor, constant: 5).isActive = true
        labelAddress.rightAnchor.constraint(equalTo: viewTop.rightAnchor, constant: -5).isActive = true
        labelAddress.heightAnchor.constraint(greaterThanOrEqualToConstant: 50).isActive = true
        
        labelRinger.topAnchor.constraint(equalTo: labelAddress.bottomAnchor).isActive = true
        labelRinger.leftAnchor.constraint(equalTo: viewTop.leftAnchor).isActive = true
        labelRinger.widthAnchor.constraint(equalToConstant: viewWidht / 2).isActive = true
        labelRinger.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        
        labelModeRinger.topAnchor.constraint(equalTo: labelAddress.bottomAnchor).isActive = true
        labelModeRinger.rightAnchor.constraint(equalTo: viewTop.rightAnchor).isActive = true
        labelModeRinger.widthAnchor.constraint(equalToConstant: viewWidht / 2).isActive = true
        labelModeRinger.heightAnchor.constraint(greaterThanOrEqualToConstant: 20).isActive = true
        
        divider.bottomAnchor.constraint(equalTo: viewTop.bottomAnchor).isActive = true
        divider.centerXAnchor.constraint(equalTo: viewTop.centerXAnchor).isActive = true
        divider.widthAnchor.constraint(equalToConstant: viewWidht / 3).isActive = true
        divider.heightAnchor.constraint(greaterThanOrEqualToConstant: 2).isActive = true

        NotificationCenter.default.addObserver(self, selector: #selector(applicationEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
    }
    
    @objc func applicationEnterBackground(){
        start()
    }
    
    func start(){
        if(CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestAlwaysAuthorization()
        }
    }
    
    @objc func restart (){
        timer?.invalidate()
        timer = nil
        start()
    }
    
    func setupMap(){
        cameraPosition = GMSCameraPosition.camera(withLatitude: 0.0, longitude: 0.0, zoom: 17)
        mapView = GMSMapView.map(withFrame: .zero, camera: cameraPosition!)
        view = mapView
        mapView?.delegate = self
        locationManager.delegate = self
        requestNearbyPlace(radius: 1000)
    }
    
    func beginNewBackgroundTask(){
        var previousTaskId = currentBgTaskId;
        currentBgTaskId = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            print("task expired: ")
        })
        if let taskId = previousTaskId{
            UIApplication.shared.endBackgroundTask(taskId)
            previousTaskId = UIBackgroundTaskIdentifier.invalid
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: self.backgroundTimer, target: self, selector: #selector(self.restart), userInfo: nil, repeats: false)
    }
    
    func requestNearbyPlace(radius: Int){
        NetworkManager.requestNearbyPlace(radius: radius){ (placeGoogle) in
            self.listPlace = placeGoogle?.result ?? self.listPlace
            if radius == 20 {
                print("listCount ", self.listPlace.count)
                if self.listPlace.count > 0 {
                    if self.previousVolume > 0.0 {
                        MPVolumeView.setVolume(0.0)
                        self.previousVolume = 0.0
                    }
                    self.labelModeRinger.text = "Silent"
                    self.labelModeRinger.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
                } else {
                    let volume = AVAudioSession.sharedInstance().outputVolume
                    if self.previousVolume != volume {
                        MPVolumeView.setVolume(volume)
                        self.previousVolume = volume
                    }
                    self.labelModeRinger.text = "Normal"
                    self.labelModeRinger.textColor = #colorLiteral(red: 0.2078431373, green: 0.5647058824, blue: 0.9176470588, alpha: 1)
                }
            } else {
                self.listPlace.forEach({ (place) in
                    let marker = PlaceMarker(place: place)
                    marker.map = self.mapView
                })
            }
        }
    }
    
    func reverseGeocodeCoordinate(_ coordinate: CLLocationCoordinate2D) {
        let geocoder = GMSGeocoder()
        geocoder.reverseGeocodeCoordinate(coordinate) { (response, error) in
            guard let address = response?.firstResult(), let lines = address.lines else {
                return
            }
            print(lines.joined(separator: "\n"))
            self.labelAddress.text = lines.joined(separator: "\n")
            
            UIView.animate(withDuration: 0.25, animations: {
                self.view.layoutIfNeeded()
            })
        }
        
    }
}
