//
//  NearByPlaceCollectionViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 22/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class NearByPlaceCollectionViewController: BasePlaceCollectionViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.async {
            self.fetchPlace1()
        }
    }
    
    func fetchPlace1() {
        //super.fetchPlace()
        let progress = ViewUtil.showDefaultProgressHud()
        let defaultRadius = 1000
        NetworkManager.requestNearbyPlace(radius: defaultRadius) { placeGoogle in
            self.listPlace.removeAll()
            let dispatchGroup = DispatchGroup()
            let dispatchQueue = DispatchQueue(label: "taskQueue")
            let dispatchSemaphore = DispatchSemaphore(value: 0)
            
            let listPlace = placeGoogle?.result ?? []
            dispatchQueue.async {
                listPlace.forEach({ (place) in
                    dispatchGroup.enter()
                    let location = place.geometry?.location
                    DatabaseManager.fetchMyLastLocation(closure: { (myLocation) in
                        NetworkManager.requestDistance(myLocation: myLocation!, location: location!, closure: { (destination) in
                            place.address = destination?.address
                            place.distanceText = destination?.distanceText
                            place.distanceValue = destination?.distanceValue ?? 0
                            place.durationText = destination?.durationText
                            place.durationValue = destination?.durationValue ?? 0
                            self.listPlace.append(place)
                            
                            dispatchSemaphore.signal()
                            dispatchGroup.leave()
                        })
                    })
                    dispatchSemaphore.wait()
                })
                DatabaseManager.fetchAllIdFavouritePlaces(closure: { (listIdPlace) in
                    self.listIdFavourite.removeAll()
                    self.listIdFavourite = listIdPlace ?? self.listIdFavourite
                    self.listPlace = self.listPlace.sorted(by: { (place0, place1) -> Bool in
                        return place0.distanceValue < place1.distanceValue
                    })
                })
            }
            
            dispatchGroup.notify(queue: dispatchQueue) {
                DispatchQueue.main.async {
                    self.collectionView.reloadData()
                    progress.hideHUD()
                }
            }
        }
    }
    
    override func saveFavourite(view: UIView) {
        let index = view.tag
        let place = listPlace[index]
        let cell = collectionView.cellForItem(at: IndexPath(item: index, section: 0)) as! PlaceCollectionViewCell
        DispatchQueue.main.async {
            if self.listIdFavourite.contains(place.id!) {
                let alertAction = UIAlertAction(title: "Yes", style: .default, handler: { (alertAction) in
                    UIView.transition(with: cell.imageFavourite, duration: 0.4, options: .transitionFlipFromLeft, animations: {
                        cell.imageFavourite.image = UIImage(named: "border_favourite")
                    }, completion: { (isComplete) in
                        DatabaseManager.removeFavouritePlace(place: place, completion: {
                            self.listIdFavourite = self.listIdFavourite.filter { $0 != place.id}
                        })
                    })
                })
                ViewUtil.customAlert(message: BankString.confirmToDelete, controller: self, alertAction: alertAction)
            } else {
                UIView.transition(with: cell.imageFavourite, duration: 0.4, options: .transitionFlipFromLeft, animations: {
                    cell.imageFavourite.image = UIImage(named: "favourite")
                }, completion: { (isComplete) in
                    if isComplete {
                        DatabaseManager.saveFavouritePlace(place: place, completion: {
                            self.listIdFavourite.append(place.id!)
                        })
                    }
                })
            }
        }
    }
}
