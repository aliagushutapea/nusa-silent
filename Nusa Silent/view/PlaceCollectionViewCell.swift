//
//  NearbyCollectionViewCell.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 21/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class PlaceCollectionViewCell: UICollectionViewCell {
    
    var nearByPlaceCollection: BasePlaceCollectionViewController?
    
    let viewContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let imageReference: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = UIView.ContentMode.scaleToFill
        return iv
    }()
    
    let labelName: UILabel = {
        let lb = UILabel()
        lb.text = BankString.name
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        return lb
    }()
    
    let valueName: UILabel = {
        let lb = UILabel()
        lb.font = UIFont.boldSystemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        return lb
    }()
    
    let labelRating: UILabel = {
        let lb = UILabel()
        lb.text = BankString.rating
        lb.font = UIFont.systemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)
        return lb
    }()
    
    let valueRating: UILabel = {
        let lb = UILabel()
        lb.font = UIFont.boldSystemFont(ofSize: 14)
        lb.translatesAutoresizingMaskIntoConstraints = false
        lb.textColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        return lb
    }()
    
    lazy var viewFavourite: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 25
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(saveFavourite)))
        return view
    }()
    
    lazy var imageFavourite: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "border_favourite"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = UIView.ContentMode.scaleAspectFill
        iv.isUserInteractionEnabled = true
        //iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(saveFavourite)))
        return iv
    }()
    
    lazy var viewRoute: UIView = {
        let view = UIView()
        view.backgroundColor = #colorLiteral(red: 0.2196078449, green: 0.007843137719, blue: 0.8549019694, alpha: 1)
        view.layer.cornerRadius = 25
        view.translatesAutoresizingMaskIntoConstraints = false
        view.isUserInteractionEnabled = true
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigateToRoute)))
        return view
    }()
    
    lazy var imageRoute: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "direction"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = UIView.ContentMode.scaleAspectFill
        iv.isUserInteractionEnabled = true
        //iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(navigateToRoute)))
        return iv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupView(){
        addSubview(viewContainer)
        viewContainer.addSubview(imageReference)
        viewContainer.addSubview(labelName)
        viewContainer.addSubview(valueName)
        viewContainer.addSubview(labelRating)
        viewContainer.addSubview(valueRating)
        viewContainer.addSubview(viewFavourite)
        viewContainer.addSubview(viewRoute)
        
        viewFavourite.addSubview(imageFavourite)
        viewRoute.addSubview(imageRoute)
        
        viewContainer.topAnchor.constraint(equalTo: topAnchor, constant: 20).isActive = true
        viewContainer.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        viewContainer.leftAnchor.constraint(equalTo: leftAnchor, constant: 15).isActive = true
        viewContainer.rightAnchor.constraint(equalTo: rightAnchor, constant: -15).isActive = true
        
        imageReference.topAnchor.constraint(equalTo: viewContainer.topAnchor).isActive = true
        imageReference.heightAnchor.constraint(equalToConstant: 280).isActive = true
        imageReference.leftAnchor.constraint(equalTo: viewContainer.leftAnchor).isActive = true
        imageReference.rightAnchor.constraint(equalTo: viewContainer.rightAnchor).isActive = true
        
        labelName.topAnchor.constraint(equalTo: imageReference.bottomAnchor, constant: 10).isActive = true
        labelName.heightAnchor.constraint(equalToConstant: 16).isActive = true
        labelName.leftAnchor.constraint(equalTo: viewContainer.leftAnchor, constant: 15).isActive = true
        labelName.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -15).isActive = true
        
        valueName.topAnchor.constraint(equalTo: labelName.bottomAnchor).isActive = true
        valueName.heightAnchor.constraint(equalToConstant: 16).isActive = true
        valueName.leftAnchor.constraint(equalTo: viewContainer.leftAnchor, constant: 15).isActive = true
        valueName.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -15).isActive = true
        
        labelRating.topAnchor.constraint(equalTo: valueName.bottomAnchor, constant: 10).isActive = true
        labelRating.heightAnchor.constraint(equalToConstant: 16).isActive = true
        labelRating.leftAnchor.constraint(equalTo: viewContainer.leftAnchor, constant: 15).isActive = true
        labelRating.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -15).isActive = true
        
        valueRating.topAnchor.constraint(equalTo: labelRating.bottomAnchor).isActive = true
        valueRating.heightAnchor.constraint(equalToConstant: 16).isActive = true
        valueRating.leftAnchor.constraint(equalTo: viewContainer.leftAnchor, constant: 15).isActive = true
        valueRating.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -15).isActive = true
        
        viewFavourite.bottomAnchor.constraint(equalTo: imageReference.bottomAnchor, constant: -20).isActive = true
        viewFavourite.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -20).isActive = true
        viewFavourite.heightAnchor.constraint(equalToConstant: 50).isActive = true
        viewFavourite.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        imageFavourite.centerXAnchor.constraint(equalTo: viewFavourite.centerXAnchor).isActive = true
        imageFavourite.centerYAnchor.constraint(equalTo: viewFavourite.centerYAnchor).isActive = true
        imageFavourite.heightAnchor.constraint(equalToConstant: 20).isActive = true
        imageFavourite.widthAnchor.constraint(equalToConstant: 20).isActive = true

        viewRoute.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor, constant: -20).isActive = true
        viewRoute.rightAnchor.constraint(equalTo: viewContainer.rightAnchor, constant: -20).isActive = true
        viewRoute.heightAnchor.constraint(equalToConstant: 50).isActive = true
        viewRoute.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
        imageRoute.centerXAnchor.constraint(equalTo: viewRoute.centerXAnchor).isActive = true
        imageRoute.centerYAnchor.constraint(equalTo: viewRoute.centerYAnchor).isActive = true
        imageRoute.heightAnchor.constraint(equalToConstant: 20).isActive = true
        imageRoute.widthAnchor.constraint(equalToConstant: 20).isActive = true
    }
    
    @objc func saveFavourite(gesture: UITapGestureRecognizer){
        let view = (gesture.view)!
        nearByPlaceCollection?.saveFavourite(view: view)
    }
    
    @objc func navigateToRoute(gesture: UITapGestureRecognizer){
        let view = (gesture.view)!
        nearByPlaceCollection?.navigateToRoute(view: view)
    }
}
