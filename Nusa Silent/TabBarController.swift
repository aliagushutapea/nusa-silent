//
//  TabBarController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 20/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.navigationItem.title = BankString.map
        
        
        let mapController = MapViewController()
        mapController.tabBarItem = UITabBarItem(title: BankString.map, image: UIImage(named: "map_deactive"), selectedImage: UIImage(named: "map"))
        let layout0 = UICollectionViewFlowLayout()
        let nearbyController = NearByPlaceCollectionViewController(collectionViewLayout: layout0)
        nearbyController.tabBarItem = UITabBarItem(title: BankString.nearby, image: UIImage(named: "nearby_deactive"), selectedImage: UIImage(named: "nearby"))
        
        let layout1 = UICollectionViewFlowLayout()
        let favouriteController = FavouriteCollectionViewController(collectionViewLayout: layout1)
        favouriteController.tabBarItem = UITabBarItem(title: BankString.favourite, image: UIImage(named: "favourite_deactive"), selectedImage: UIImage(named: "favourite"))
        
        let tabBarList = [nearbyController, mapController, favouriteController]
        viewControllers = tabBarList
        self.selectedIndex = 1
    }

}
