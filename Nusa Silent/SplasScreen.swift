//
//  ViewController.swift
//  Nusa Silent
//
//  Created by Ali Agus Hutapea on 18/05/19.
//  Copyright © 2019 Ali Agus Hutapea. All rights reserved.
//

import UIKit

class SplashScreen: UIViewController {
    
    let imageSplash: UIImageView = {
        let iv = UIImageView(image: UIImage(named: "image_splash"))
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    func setupView(){
        self.view.backgroundColor = #colorLiteral(red: 0.1490196078, green: 0.7764705882, blue: 0.8549019608, alpha: 1)
        self.view.addSubview(imageSplash)
        
        imageSplash.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageSplash.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageSplash.heightAnchor.constraint(equalToConstant: 150).isActive = true
        imageSplash.widthAnchor.constraint(equalToConstant: 150).isActive = true
        
        let dispatchTime = DispatchTime.now() + 1
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            let uiNavigationController = UINavigationController(rootViewController: TabBarController())
            self.present(uiNavigationController, animated: true, completion: nil)
        }
    }


}

